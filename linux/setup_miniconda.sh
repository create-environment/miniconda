#!/bin/bash

hasProxy='False' # True | False
proxy='http://example.jp:8080'

if [ $hasProxy = 'True' ]; then
    conda config --set proxy_servers.http $proxy
    conda config --set proxy_servers.https $proxy
fi

conda config --remove channels defaults
conda config --add channels conda-forge
conda update conda -y

env_name='opencae'
if conda activate $env_name; then
    echo "env $env_name already exists. updating all packages..."
    conda update --all
    conda deactivate
else
    echo "env $env_name is going to be created..."
    conda create -y -n $env_name python=3.10
    conda activate $env_name
    conda install -y \
        calculix \
        dataclasses-json \
        freecad \
        matplotlib \
        optuna \
        optuna-dashboard \
        plotly \
        pandas \
        requests
fi

env_name='gui'
if conda activate $env_name; then
    echo "env $env_name already exists. updating all packages..."
    conda update --all
    conda deactivate
else
    echo "env $env_name is going to be created..."
    conda create -y -n $env_name python=3.10
    conda activate $env_name
    conda install -y pyside6 dataclasses-json
fi

echo "Everything has been done. You can close this terminal."
