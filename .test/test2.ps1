# Running as admin or not?
if (([Security.Principal.WindowsPrincipal][Security.Principal.WindowsIdentity]::GetCurrent()).IsInRole("Administrators")) { 
    # Write-Host '管理者権限で実行中です．'
    Write-Host 'running as administrator now.'
} else {
    # Write-Host 'ローカルユーザ権限で実行中です．'
    Write-Host 'running as local user now.'
}

Pause
