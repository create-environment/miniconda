Write-Host "0: local user."
Write-Host "1: global(administrator) user."
$installation_mode = Read-Host 'enter the number...'

# call next steps
if ($installation_mode -eq 0) {
    Start-Process powershell -ArgumentList "-NoExit", "-File .\test2.ps1"
    exit
}
elseif ($installation_mode -eq 1) {
    Start-Process powershell -ArgumentList "-NoExit", "-File .\test2.ps1" -Verb RunAs
    exit
}
