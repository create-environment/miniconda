# PowerShellスクリプトの途中から管理者権限に移行する

- 実行環境はWin11 Home 22H2
- コマンドが`powershell`，つまりVer5以前では，管理者権限でプロンプトに移行できない．
- `pwsh`，つまりVer7以降の新しいPowerShellを使わないと，想定した挙動をしてくれない．
    - **つまり，PowerShell7をインストールしないといけない．**