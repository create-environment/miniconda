# Run as admin
if (!([Security.Principal.WindowsPrincipal][Security.Principal.WindowsIdentity]::GetCurrent()).IsInRole("Administrators")) { 
    Start-Process pwsh "-File `"$PSCommandPath`"" -Verb RunAs;
    exit
}

# 1. Load the Miniconda environment
$installation_mode = -1
. $PSScriptRoot\load_miniconda.ps1

# 2. Activate the 'base' Python environment
conda activate

# 3. Create a new virtual Python environment with ver.3.11
$env_name = 'test'

if ($installation_mode -eq 0) {
    conda create -y -n $env_name python=3.11
} elseif ($installation_mode -eq 1) {
    conda create -y -p C:\ProgramData\miniconda3\envs\$env_name python=3.11
} else {
    Write-Host '*** WARNING ***  Something wrong happend.'
    Pause
    exit
}

# 4. Install packages in this environmet
conda activate $env_name
conda install -y numpy
# conda install -y matplotlib
# and so on

Write-Host 'Everything has been done! You can close the terminal.'
Pause
 