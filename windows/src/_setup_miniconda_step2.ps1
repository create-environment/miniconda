# Source PowerShell script
$dir_pwd = $PSScriptRoot # /src
. $dir_pwd\proxy_setting.ps1

# Running as admin or not?
if (([Security.Principal.WindowsPrincipal][Security.Principal.WindowsIdentity]::GetCurrent()).IsInRole("Administrators")) { 
    # Write-Host '管理者権限で実行中です．'
    Write-Host 'running as administrator now.'
} else {
    # Write-Host 'ローカルユーザ権限で実行中です．'
    Write-Host 'running as local user now.'
}

$installation_mode = 'None'
. $dir_pwd\load_miniconda.ps1

if ($hasProxy) {
    # Write-Host 'Minicondaのプロキシ設定を行っています...'
    Write-Host 'Configuring Miniconda proxy...'
    conda config --set proxy_servers.http $proxy
    conda config --set proxy_servers.https $proxy
}

# Write-Host 'ライセンス違反を回避するため，Minicondaのチャネル設定を行っています...'
Write-Host 'Configuring Miniconda channels to avoid license violations...'
try {
    conda config --remove channels defaults
    conda config --add channels conda-forge
}
catch {
    Write-Host '  Already configured.'
}

# Write-Host 'Minicondaのアップデートを開始します...'
Write-Host 'The update of Miniconda is starting...'
conda update conda -y
# Write-Host 'Minicondaのアップデートが完了しました！'
Write-Host 'The update of Miniconda is completed!'



Write-Host ''
# Write-Host '-------- STEP 2. Python仮想環境の作成 --------'
Write-Host '-------- STEP 2. Creation of virtual Python environments --------'
Write-Host ''


$create_envs = $true # $true | $false

if ($create_envs) {
    # Write-Host "環境'opencae'の新規作成を開始します..."
    Write-Host "The creation of an environment 'opencae' is starting..."
    if ($installation_mode -eq 'LOCALLY') {
        conda create -y -n opencae python=3.10
    } elseif ($installation_mode -eq 'GLOBALLY') {
        conda create -y -p C:\ProgramData\miniconda3\envs\opencae python=3.10
    } else {
        $error_message = ''
        $error_message += "*** ERROR ***  Something wrong with '$installation_mode'. `r`n"
        $error_message += "`r`n"
        $error_message += "(You can copy this message to the clipboard by Ctrl+C) `r`n"
    
        Add-Type -AssemblyName System.Windows.Forms;
        [System.Windows.Forms.MessageBox]::Show($error_message, 'ERROR')
        exit
    }    
    conda activate opencae
    conda install -y freecad
    conda install -y optuna
    conda install -y optuna-dashboard
    conda install -y matplotlib
    conda install -y plotly
    conda install -y pandas
    conda install -y requests
    # Write-Host "環境'opencae'の新規作成が完了しました！"
    Write-Host "The creation of an environment 'opencae' is completed!"
    

    $env_name = 'pyside'
    # Write-Host "環境$env_nameの新規作成を開始します..."
    Write-Host "The creation of an environment $env_name is starting..."
    if ($installation_mode -eq 'LOCALLY') {
        conda create -y -n $env_name
    } elseif ($installation_mode -eq 'GLOBALLY') {
        conda create -y -p C:\ProgramData\miniconda3\envs\$env_name
    } else {
        $error_message = ''
        $error_message += "*** ERROR ***  Something wrong with '$installation_mode'. `r`n"
        $error_message += "`r`n"
        $error_message += "(You can copy this message to the clipboard by Ctrl+C) `r`n"
    
        Add-Type -AssemblyName System.Windows.Forms;
        [System.Windows.Forms.MessageBox]::Show($error_message, 'ERROR')
        exit
    }
    conda activate $env_name
    conda install -y pip
    if ($hasProxy) {
        pip install --proxy=$proxy pyside6
    } else {
        pip install pyside6
    }
    # Write-Host "環境'pyside'の新規作成が完了しました！"
    Write-Host "The creation of an environment 'pyside' is completed!"
}

Write-Host '-----------------------------------------------------------------'
Write-Host ''
# Write-Host 'すべての作業が完了しました！'
Write-Host 'Everything has been done!'
Pause
