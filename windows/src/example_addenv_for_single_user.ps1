# 1. Load the Miniconda environment

. $PSScriptRoot\load_miniconda.ps1


# 2. Create a new virtual Python environment with ver.3.10

$env_name = 'test' # <-- CHANGE this name as you wish.

try {
    conda activate $env_name
    Write-Host "env $env_name already exists. Updating all packages..."
    conda update --all
    conda deactivate
    Write-Host 'All packages have been updated! You can close this terminal.'
}
catch {   
    Write-Host "Creating env $env_name ..."
    conda create -y -n $env_name python=3.10 # <-- CHANGE this version as you wish.
    conda activate $env_name

    conda install -y `
        numpy `
        matplotlib `
        requests     # <-- CHANGE these packages as you wish.

    Write-Host 'All packages have been installed! You can close this terminal.'
}