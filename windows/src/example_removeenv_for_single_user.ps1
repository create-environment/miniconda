# 1. Load the Miniconda environment

. $PSScriptRoot\load_miniconda.ps1


# 2. Activate the 'base' Python environment

conda activate


# 3. Remove an existing virtual Python environment

$env_name = 'test' # <-- CHANGE this name as you wish.

Write-Host "Creating env $env_name ..."
try {
    conda remove -y -n $env_name --all
}
catch {
    Write-Host '*** WARNING ***  Something wrong happend.'
    Pause
    exit
}

Write-Host "Environment $env_name have been removed! You can close this terminal."
 