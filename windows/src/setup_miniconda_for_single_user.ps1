# Source pwsh script
$dir_pwd = $PSScriptRoot # /src
. $dir_pwd\proxy_setting.ps1

if ($hasProxy) {
    if ($proxy -eq 'http://example.jp:8080') {
        $error_message = ''
        $error_message += "Proxy setting has not modified! `r`n"
        $error_message += "You should modify '$proxy' variable in the first line of `r`n"
        $error_message += "miniconda/windows/src/proxy_setting.ps1 properly `r`n"
        $error_message += "and re-execute the batch. `r`n"
        $error_message += "`r`n"
        $error_message += "(You can copy this message to the clipboard by Ctrl+C) `r`n"

        Add-Type -AssemblyName System.Windows.Forms;
        [System.Windows.Forms.MessageBox]::Show($error_message, 'ERROR')

        exit
    }
}

# Write-Host '-------- STEP 1. Minicondaのインストール --------'
Write-Host '-------- STEP 1. installation of Miniconda --------'
Write-Host ''

$miniconda_path_candidates = @{
    'SingleUser' = "${env:userprofile}\AppData\Local\miniconda3"
    'AllUsers' = "C:\ProgramData\miniconda3"
}

$isInstalled = $false
$installation_mode = ''
foreach ($key in $miniconda_path_candidates.Keys) {
    if (Test-Path $miniconda_path_candidates[$key]) {
        # Write-Host "Minicondaは既に $key にインストールされています. ショートカットファイルはこちらです:"
        Write-Host "Miniconda has already been installed for $key , and located in:"
        Write-Host $miniconda_path_candidates[$key]
        $installation_mode = $key
        $isInstalled = $true
    }
}

if ($installation_mode -eq 'AllUsers') {
    $error_message = ''
    $error_message += "*** ERROR ***  Miniconda3 is already installed FOR ALL USERS, `r`n"
    $error_message += "located in C:\ProgramData\miniconda3. `r`n"
    $error_message += "Use 'setup_miniconda_for_global_user.bat' instead. `r`n"
    $error_message += "`r`n"
    $error_message += "(You can copy this message to the clipboard by Ctrl+C) `r`n"

    Add-Type -AssemblyName System.Windows.Forms;
    [System.Windows.Forms.MessageBox]::Show($error_message, 'ERROR')
    exit
}

# function cannot_load_miniconda() {
#     Out-File -InputObject $error[0] -FilePath error.txt -Append

#     $error_message = ''
#     $error_message += "Miniconda could not be activated! Error message is below: `r`n"
#     $error_message += "$error[0] `r`n"
#     $error_message += "`r`n"
#     $error_message += "(You can copy this message to the clipboard by Ctrl+C) `r`n"

#     Add-Type -AssemblyName System.Windows.Forms;
#     [System.Windows.Forms.MessageBox]::Show($error_message, 'ERROR')

#     exit
# }

if (-not $isInstalled) {
    # Write-Host 'Minicondaはこのコンピュータにはインストールされていません．インストールを開始します...'
    Write-Host 'Miniconda is not installed in this computer. The installation is starting...'

    winget install Anaconda.Miniconda3 --location "${env:userprofile}\AppData\Local\miniconda3"
    
    Write-Host ''
    # Write-Host 'Minicondaのインストールが完了しました！続いて，Minicondaを起動します.'
    Write-Host 'The installation of Miniconda is completed! The next step is to activate it.'
}

. $dir_pwd\load_miniconda.ps1

if ($hasProxy) {
    # Write-Host 'Minicondaのプロキシ設定を行っています...'
    Write-Host 'Configuring Miniconda proxy...'
    conda config --set proxy_servers.http $proxy
    conda config --set proxy_servers.https $proxy
}

# Write-Host 'ライセンス違反を回避するため，Minicondaのチャネル設定を行っています...'
Write-Host 'Configuring Miniconda channels to avoid license violations...'
try {
    conda config --remove channels defaults
    conda config --add channels conda-forge
}
catch {
    Write-Host 'Channels has already been configured.'
}

# Write-Host 'Minicondaのアップデートを開始します...'
Write-Host 'Updating Miniconda...'
conda update conda -y
# Write-Host 'Minicondaのアップデートが完了しました！'
Write-Host 'The update is completed!'

Write-Host ''
Write-Host ''
# Write-Host '-------- STEP 2. Python仮想環境の作成 --------'
Write-Host '-------- STEP 2. Creation of virtual Python environments --------'
Write-Host ''

$env_name = 'gui'
try {
    conda activate $env_name
    Write-Host "env $env_name already exists. Updating all packages..."
    conda update --all
    conda deactivate
}
catch {    
    # Write-Host "環境$env_nameの新規作成を開始します..."
    Write-Host "An environment $env_name is going to be created..."
    
    conda create -y -n $env_name python=3.10
    conda activate $env_name
    conda install -y pyside6 dataclasses-json
    
    # Write-Host "環境'pyside'の新規作成が完了しました！"
    Write-Host "The creation of an environment '$env_name' is completed!"
}

$create_env_opencae = $false # $true | $false
if ($create_env_opencae) {
    $env_name = 'opencae'
    try {
        conda activate $env_name
        Write-Host "env $env_name already exists. Updating all packages..."
        conda update --all
        conda deactivate
    }
    catch {    
        # Write-Host "環境'opencae'の新規作成を開始します..."
        Write-Host "An environment $env_name is going to be created..."
        conda create -y -n $env_name python=3.10
        conda activate $env_name
        conda install -y `
            calculix `
            dataclasses-json `
            freecad `
            matplotlib `
            optuna `
            optuna-dashboard `
            plotly `
            pandas `
            requests
        # Write-Host "環境'opencae'の新規作成が完了しました！"
        Write-Host "The creation of an environment 'opencae' is completed!"
    }
}

Write-Host '-----------------------------------------------------------------'
Write-Host ''
# Write-Host 'すべての作業が完了しました！'
Write-Host 'Everything has been done! You can close this terminal.'
