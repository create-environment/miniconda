try {
    & ${env:userprofile}\AppData\Local\miniconda3\shell\condabin\conda-hook.ps1 -ErrorAction Stop
    # $installation_mode = 'SingleUser'
    Write-Host "Miniconda is installed in ${env:userprofile}\AppData\Local\miniconda3"
}
catch {
    try {
        & C:\ProgramData\miniconda3\shell\condabin\conda-hook.ps1 -ErrorAction Stop
        # $installation_mode = 'AllUsers'
        Write-Host "Miniconda is installed in C:\ProgramData\miniconda3"
    }
    catch {
        $error_message = ''
        $error_message += "Miniconda is not installed! `r`n"
        $error_message += "Execute 'setup_miniconda.bat' first. `r`n"
        $error_message += "    See: https://gitlab.com/create-environment/miniconda/-/tree/main/#2-windows%E3%81%A7%E3%81%AE%E7%92%B0%E5%A2%83%E6%A7%8B%E7%AF%89 `r`n"
        $error_message += "`r`n"
        $error_message += "(You can copy this message to the clipboard by Ctrl+C) `r`n"

        Add-Type -AssemblyName System.Windows.Forms;
        [System.Windows.Forms.MessageBox]::Show($error_message, 'ERROR')
        
        exit
    }
}

conda activate
