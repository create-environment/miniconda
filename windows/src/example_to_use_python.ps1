Set-Location $PSScriptRoot

# 1. Load the Miniconda environment 
. $PSScriptRoot\load_miniconda.ps1 # windows\src\load_miniconda.ps1

# 2. Activate the virtual Python environment
$env_name = 'gui'
try {
    conda activate $env_name
}
catch {
    $error_message = ''
    $error_message += "Virtual environment $env_name is not found! `r`n"
    $error_message += "Create the environment. `r`n"
    $error_message += "    See: https://gitlab.com/create-environment/miniconda/-/blob/main/windows/src/example_addenv.ps1 `r`n"
    $error_message += "`r`n"
    $error_message += "(You can copy this message to the clipboard by Ctrl+C) `r`n"

    Add-Type -AssemblyName System.Windows.Forms;
    [System.Windows.Forms.MessageBox]::Show($error_message, 'ERROR')
    
    exit
}

# 3. Execute the Python programs
python main.py
 