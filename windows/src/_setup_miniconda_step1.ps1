# Source pwsh script
$dir_pwd = $PSScriptRoot # /src
. $dir_pwd\proxy_setting.ps1

if ($hasProxy) {
    if ($proxy -eq 'http://example.jp:8080') {
        $error_message = ''
        $error_message += "Proxy setting has not modified! `r`n"
        $error_message += "You should set '$proxy' variable in the first line of `r`n"
        $error_message += "miniconda/windows/src/proxy_setting.ps1 properly `r`n"
        $error_message += "and re-execute the batch. `r`n"
        $error_message += "`r`n"
        $error_message += "(You can copy this message to the clipboard by Ctrl+C) `r`n"

        Add-Type -AssemblyName System.Windows.Forms;
        [System.Windows.Forms.MessageBox]::Show($error_message, 'ERROR')

        exit
    }
}

Write-Host ''
Write-Host ''
Write-Host ''
Write-Host ''
Write-Host ''
Write-Host ''
Write-Host ''
# Write-Host '-------- STEP 1. Minicondaがインストールされているかの確認 --------'
Write-Host '-------- STEP 1. Confirmation of Miniconda installation --------'
Write-Host ''

$shortcutpath_candidates = @{
    'LOCALLY' = "${env:userprofile}\AppData\Roaming\Microsoft\Windows\Start Menu\Programs\Miniconda3 (64-bit)"
    'GLOBALLY' = 'C:\ProgramData\Microsoft\Windows\Start Menu\Programs\Miniconda3 (64-bit)'
}

$isInstalled = $false
foreach ($key in $shortcutpath_candidates.Keys) {
    if (Test-Path $shortcutpath_candidates[$key]) {
        # Write-Host "Minicondaは既に $key にインストールされています. ショートカットファイルはこちらです:"
        Write-Host "Miniconda has already been installed $key . The Shortcut file is here:"
        Write-Host $shortcutpath_candidates[$key]
        $installation_mode = $key
        $isInstalled = $true
    }
}

function cannot_load_miniconda() {
    Out-File -InputObject $error[0] -FilePath error.txt -Append

    $error_message = ''
    $error_message += "Miniconda could not be activated! Error message is below: `r`n"
    $error_message += "$error[0] `r`n"
    $error_message += "`r`n"
    $error_message += "(You can copy this message to the clipboard by Ctrl+C) `r`n"

    Add-Type -AssemblyName System.Windows.Forms;
    [System.Windows.Forms.MessageBox]::Show($error_message, 'ERROR')

    exit
}

if ($isInstalled) {
    if ($installation_mode -eq 'LOCALLY') {
        # Write-Host "Minicondaを起動するため，'${env:userprofile}\AppData\Local\miniconda3\shell\condabin\conda-hook.ps1'を実行しています..."
        Write-Host "In order to activate Miniconda, executing '${env:userprofile}\AppData\Local\miniconda3\shell\condabin\conda-hook.ps1'..."
        try {
            & ${env:userprofile}\miniconda3\shell\condabin\conda-hook.ps1 -ErrorAction Stop
        }
        catch {
            try {
                & ${env:userprofile}\AppData\Local\miniconda3\shell\condabin\conda-hook.ps1 -ErrorAction Stop
            }
            catch {
                cannot_load_miniconda
            }
        }
    } elseif ($installation_mode -eq 'GLOBALLY') {
        # Write-Host "Minicondaを起動するため，'C:\ProgramData\miniconda3\shell\condabin\conda-hook.ps1'を実行しています..."
        Write-Host "In order to activate, executing 'C:\ProgramData\miniconda3\shell\condabin\conda-hook.ps1'..."
        try {
            & C:\ProgramData\miniconda3\shell\condabin\conda-hook.ps1 -ErrorAction Stop
        }
        catch {
            cannot_load_miniconda
        }
    } else {
        $error_message = ''
        $error_message += "*** ERROR ***  Something wrong happend. `r`n"
        $error_message += "`r`n"
        $error_message += "(You can copy this message to the clipboard by Ctrl+C) `r`n"
    
        Add-Type -AssemblyName System.Windows.Forms;
        [System.Windows.Forms.MessageBox]::Show($error_message, 'ERROR')
        exit
    }    
} else {
    # Write-Host 'Minicondaはこのコンピュータにはインストールされていません．インストールを開始します...'
    Write-Host 'Miniconda is not installed in this computer. The installation is starting...'
    
    Write-Host '/------------------------N O T I O N------------------------\'
    # Write-Host '|  まもなくインストールウィザードが起動します．ウィザードでは，  |'
    # Write-Host '|  利用者に応じてインストール方式を次のように選べます:          |'
    Write-Host '|  The INSTALLATION WIZARD will open soon. In that wizard,  |'
    Write-Host '|  you can choose the install mode like below:              |'
    Write-Host '|                                                           |'
    Write-Host '|    Install for:                                           |'
    Write-Host '|      (x) Just Me (recommended)                            |'
    Write-Host '|      ( ) All Users (requires admin privileges)            |'
    Write-Host '|                                                           |'
    Write-Host '\-----------------------------------------------------------/'

    Invoke-WebRequest -Uri https://repo.anaconda.com/miniconda/Miniconda3-latest-Windows-x86_64.exe -OutFile ${env:userprofile}\Downloads\Miniconda3-latest-Windows-x86_64.exe
    Start-Process -FilePath "${env:userprofile}\Downloads\Miniconda3-latest-Windows-x86_64.exe" -Wait
    Remove-Item ${env:userprofile}\Downloads\Miniconda3-latest-Windows-x86_64.exe

    Write-Host ''
    # Write-Host 'Minicondaのインストールが完了しました！続いて，Minicondaを起動します.'
    Write-Host 'The installation of Miniconda is completed! The next step is to activate it.'
    $installation_mode = 'None'
    . $dir_pwd\load_miniconda.ps1
}

# call next steps

if ($installation_mode -eq 'LOCALLY') { 
    Start-Process pwsh -ArgumentList "-File setup_miniconda_step2.ps1"
    exit
}
elseif ($installation_mode -eq 'GLOBALLY') {
    # Write-Host '全ユーザを対象にインストールしたため，ここからの作業には管理者権限が必要です．'
    # Write-Host 'この後，プログラムの実行可否を尋ねる画面が表示されるので，許可してください．'
    # Write-Host 'よろしければ，任意のキーを押してEnterしてください...'
    Write-Host "Since you have chosen the install mode 'All Users' , we request the authority of administrator"
    Write-Host "You will see the prompt about that later."
    Read-Host 'If it is OK with you, Press any key and enter...'
    Start-Process pwsh -ArgumentList "-File setup_miniconda_step2.ps1" -Verb RunAs
    exit
}
else {
    $error_message = ''
    $error_message += "*** ERROR ***  Something wrong happend. `r`n"
    $error_message += "`r`n"
    $error_message += "(You can copy this message to the clipboard by Ctrl+C) `r`n"

    Add-Type -AssemblyName System.Windows.Forms;
    [System.Windows.Forms.MessageBox]::Show($error_message, 'ERROR')
    exit
}