# Miniconda環境構築

AUTHOR: M.TANAKA  
CREATE: 2023.01.10  
UPDATE: 2023.09.22  

[TOC]

## 1. 趣旨

個人的な探索活動で利用するMinicondaの環境構築方法を記録する．Minicondaの利用目的には以下のようなものが挙げられる．

- Qt for Python (PySide) を用いたGUIアプリケーション開発
- FreeCADを用いたジオメトリ生成
- Calculixを用いた構造解析
- optunaを用いた最適化
- 等々

### 1.1 pipではなくcondaを使う理由

仮想環境をいくつも作れて，後述するように商用利用も可能であることから選んだ．（調べていないだけでpipのvenvでも同じかもしれないが）

### 1.2 AnacondaでなくMinicondaを使う理由

Anacondaは個人的利用においては無償である．しかし実際問題，探索活動は業務に活用することも念頭において行っているため，仮に業務利用するとしてもライセンス違反の起こらないようにしたい．そのためMinicondaおよびconda-forgeチャネルの構成を採用する．

- 参考：[【Qiita】Anacondaの有償化に伴いminiconda+conda-forgeでの運用を考えてみた](https://qiita.com/kimisyo/items/986802ea52974b92df27)

## 2. Windowsでの環境構築

### 2.1 クイックスタート

以下が，必要な全ての手順である．

#### 2.1.1 事前準備

- パッケージのインストールには`winget`を使う．Windows11であれば標準で用意されているが，Windows10だと`winget`自体がインストールされていないかもしれない．

    - 確認方法：コマンドプロンプトを開き，下記コマンドを実行．
        ```
        C:\Users\***> winget --help
        ```

    - `winget`の使用方法が表示されればインストール済み．もし`winget: The term 'winget' is not recognized...`などと表示された場合，`winget`がインストールされていない．下記ページを参考にインストールする．

    - [winget ツールを使用したアプリケーションのインストールと管理](https://learn.microsoft.com/ja-jp/windows/package-managerwinget)

- 自動化にはPowerShell7(`pwsh`)を使う．こちらもWindows11では標準だが，Windows10ではPowerShell5(`powershell`)が標準なので，別途インストールが必要かもしれない．

    - 確認方法：コマンドプロンプトを開き，下記コマンドを実行．

        ```
        C:\Users\***> pwsh --help
        ```

    - `pwsh`の使用方法が表示されればインストール済み．もし`pwsh: The term 'pwsh' is not recognized...`などと表示された場合，PowerShwll7がインストールされていない．下記コマンドでインストールする．

        ```
        C:\Users\***> winget install --id Microsoft.Powershell --source winget
        ```

以上で事前の準備は完了した．

#### 2.1.2 Minicondaのインストール

- 画像のように，このページ上部のボタンからレポジトリのzipをダウンロードし，任意の場所に展開する．`miniconda-main`というフォルダができる．
    ![](figs/download_gitlab_miniconda.png)

1. プロキシ環境の設定

    - 当該コンピュータがプロキシ環境下にない場合，この項目は無視してよい．ある場合はまず，`miniconda-main/windows/src/proxy_setting.ps1`の内容を環境に応じて変更する．

        ```powershell
        # BEFORE
        $hasProxy = $false # $true | $false
        $proxy = 'http://example.jp:8080'
        ```

        ```powershell
        # AFTER
        $hasProxy = $true # $true | $false
        $proxy = '(your-proxy-address):(port-number)'
        ```

2. 単独のユーザーが利用するコンピュータの場合

    - `miniconda-main/windows/setup_miniconda_for_single_user.bat`をダブルクリックするだけで，Minicondaのインストールから動作テスト用の仮想環境`gui`の作成，パッケージのインストールまで自動で行う．

        - なお，すでに一度実行済みの場合はアップデートのみ行うようにしている．

    - "Everything has been done!"と表示されたら完了．

3. 複数ユーザが利用するコンピュータの場合

    - 追記予定．


#### 2.1.3 Minicondaの使用例

- Pythonプログラムを実行するバッチファイルの例として`miniconda-main/windows/example_to_use_python.bat`を用意した．これをダブルクリックすると，PythonのGUIライブラリPySide6を用いたサンプルプログラムが起動する（※ただのモックなのでボタンは色々触ってOK）．

    - （開発者へ）各種Pythonプログラムをバッチ化して配布する場合の参考になれば．

    ![](figs/run_python_example.png)

<!--
    - (解説1) スタートメニューから`Anaconda Prompt` (`Anaconda PowerShell Prompt`でも良い)を開き，`conda info -e`と入力．以下のように仮想環境のリスト(`base`, `pyside`)が表示される．もともとMinicondaには`base`環境しかない．`pyside`はGUIアプリケーションを作るためのパッケージをインストールした環境で，先ほどの導入バッチの中で作成している．

        ```
        (base) >conda info -e
        # conda environments:
        #
        base                  *  (userprofile)\AppData\Local\miniconda3
        pyside                   (userprofile)\AppData\Local\miniconda3\envs\pyside
        ```

    - (解説2) `pyside`環境の中にインストールされたパッケージを確認すると，`pyside6`というものがある．`example_to_run.bat`ではこれをimportしたpythonプログラム[`miniconda/windows/src/main.py`](https://gitlab.com/create-environment/miniconda/-/blob/main/windows/src/main.py)を動かしている．

        ```
        (base) >conda activate pyside

        (pyside) >conda list
        # packages in environment at (userprofile)\AppData\Local\miniconda3\envs\pyside:
        #
        # Name                    Version                   Build  Channel
        bzip2                     1.0.8                h8ffe710_4    conda-forge
        ...
        pixman                    0.40.0               h8ffe710_0    conda-forge
        pyside6                   6.5.2           py311he36728c_0    conda-forge
        python                    3.11.5          h2628c8c_0_cpython    conda-forge
        ...
        zstd                      1.5.5                h12be248_0    conda-forge
        ```
-->

#### 2.1.4 Minicondaへの仮想環境の追加例

1. 単独のユーザーが利用するコンピュータの場合

    - Minicondaに新たなPython仮想環境を追加する例として`miniconda-main/windows/example_addenv_for_single_user.bat`を用意した．そのまま実行すると新たに`test`という環境が作成される．適宜`miniconda-main/windows/src/example_addenv_for_single_user.ps1`を編集して利用されたい．

<!--
    - (解説) スタートメニューから`Anaconda Prompt` (`Anaconda PowerShell Prompt`でも良い)を開き，`conda info -e`と入力．以下のように仮想環境のリスト(`base`, `pyside`)が表示される．上記バッチを実行すると，ここにもう一つ`test`が追加されることになる．

        ```
        (base) >conda info -e
        # conda environments:
        #
        base                  *  (userprofile)\AppData\Local\miniconda3
        pyside                   (userprofile)\AppData\Local\miniconda3\envs\pyside
        test                     (userprofile)\AppData\Local\miniconda3\envs\test
        ```
-->
    
<!-- 
    - 紐づけたPowerShellが管理者権限で開く．「このアプリがデバイスに変更を加えることを許可しますか？」に対しYESを選び，処理を進める．
    - 管理者権限が必要なのは，後述する"All Users"インストールの場合のみ．しかし場合分けして特権昇格するようなPowerShellスクリプトを書けなかったので．初めから要求している（直したい）．

- Minicondaのインストールウィザードが出てきたらそのまま進み，以下の画像のようにインストール先を選択する場面では，必要に応じた方を選択する．
    - "Just Me"：個人のユーザディレクトリにインストールするため，同じPCに他人がログインするとMinicondaは使えない．
    - "All Users"：同じPCにログインする全てのユーザが使えるが，今後パッケージのインストールに一々管理者権限が必要．

    ![](figs/installFor.png)

- 後はfinishまでデフォルトで進み，"Getting started with Conda", "Welcome to Anaconda"の２つのチェックを外して閉じれば良い．

    ![](figs/miniconda_install_finish.png)

- PowerShell画面に戻る．どちらのインストール形態を選んだか聞いてくるので，答える．その後，5分ほど処理が進むので放置． 
-->

以下，順を追って説明する（※コードの更新により不一致あり．要点は変わらない）．

### 2.2 Minicondaのインストール・チャネルの設定

`setup_miniconda_for_single_user.bat`の働きは`setup_miniconda_for_single_user.ps1`をキックするだけ（ダブルクリックで実行したいのでbatを介した）．実際の操作は全て`setup_miniconda_for_single_user.ps1`で処理する．

- `setup_miniconda_for_single_user.bat`

    ```batch
    pwsh -ExecutionPolicy ByPass -NoExit -Command ".\src\example_removeenv_for_single_user.ps1"
    ```

Minicondaがインストールされているかを確認し，存在しない場合，[wingetコマンド](https://learn.microsoft.com/en-us/windows/package-manager/winget/)を使ってMinicondaをインストールする．インストールが自動で完了する．

- `setup_miniconda_for_single_user.ps1`(抜粋)

    ```powershell
    winget install Anaconda.Miniconda3 --location "${env:userprofile}\AppData\Local\miniconda3"
    ```

- `setup_miniconda_for_all_users.ps1`(抜粋)

    - **要テスト**．管理者権限が必要なので「コンピュータに変更を加えても良いか」のポップアップにYESとする．

    ```powershell
    winget install Anaconda.Miniconda3 --location "C:\ProgramData\miniconda3"
    ```

<!-- 
- （追記）wingetがインストールされていない場合，ネットからインストーラをダウンロードして実行させる．ウィザードがポップアップするので，全ての選択をデフォルトのままにして進めるだけで良い．

    - :warning: wingetでインストールした場合と，直接インストーラを使う場合とでは，インストールされる場所が異なる．前者は`${env:userprofile}/Miniconda3`，後者は`${env:userprofile}/AppData/Local/miniconda3`．try-catchでどちらにも対応するように書く． 
-->

プロキシ環境にあるコンピュータで利用したい場合，サーバアドレスとポート番号を設定する．

- `setup_miniconda_for_single_user.ps1`(抜粋)

    ```powershell
    $proxy = '(your-proxy-address):(port-number)'
    conda config --set proxy_servers.http $proxy
    conda config --set proxy_servers.https $proxy
    ```

conda-forgeチャネルのみを利用するようにMinicondaの設定を変更する．

- `setup_miniconda_for_single_user.ps1`(抜粋)

    ```powershell
    conda config --remove channels defaults
    conda config --add channels conda-forge
    conda update conda -y
    ```

### 2.3 仮想環境の作成・パッケージのインストール

仮想環境`opencae`と`pyside`を作成し，必要なパッケージをインストールする．Minicondaコマンドは同様にPowerShellスクリプト`setup_miniconda.ps1`に記述しており，cmdから呼び出している．

- `opencae`のPythonバージョンを指定するのは，FreeCADが2023年1月時点でPython3.11に対応していないため．
- `gui`を分けて環境作成するのは，pyside6とfreecadが競合するため．

- `setup_miniconda_for_single_user.ps1`(抜粋)

    ```powershell
    conda create -y -n opencae python=3.10
    conda activate opencae
    conda install -y `
        calculix `
        dataclasses-json `
        freecad `
        matplotlib `
        optuna `
        optuna-dashboard `
        plotly `
        pandas `
        requests

    conda create -y -n gui
    conda activate gui
    conda install -y pyside6 dataclasses-json
    ```


## 3. 便利な設定（開発者向け）

### 3.1 WindowsターミナルへのMiniconda PowerShellの登録

単一ユーザで使用する場合，私は下記内容で新規作成している．

- 名前
    - Miniconda3 PowerShell7

- コマンドライン
    - `C:\Program Files\PowerShell\7\pwsh.exe -ExecutionPolicy ByPass -NoExit -Command "& %userprofile%\AppData\Local\miniconda3\shell\condabin\conda-hook.ps1; conda activate opencae"`


### 3.2 VSCodeへのターミナルの登録

VSCodeでは「Ctrl + Shift + @」でターミナルを呼び出せる．これにMiniconda3のPowerShell Promptを追加する．

VSCodeの設定を開き，"Search settings"とある検索欄に`terminal:integrated:profiles:windows`と打つと，`Settings > Terminal > Integrated > Profiles: Windows`の項目へ移動する．欄の下の方にある"Edit in settings.json"をクリックするとjsonファイルが開く．

以下のように`PowerShell`エントリと同じ階層に`Miniconda3 PowerShell7`エントリを追記する．デフォルトで`opencae`仮想環境が有効化されるようにしている．

- "**Just Me**"の場合：

    ```json
            ...
            "PowerShell": {
                "source": "PowerShell",
                "icon": "terminal-powershell"
            },
            "Miniconda3 PowerShell7": {
                "source": "PowerShell",
                "args": [
                    "-ExecutionPolicy",
                    "ByPass",
                    "-NoExit",
                    "-Command",
                    "& '${env:userprofile}\\AppData\\Local\\miniconda3\\shell\\condabin\\conda-hook.ps1' ;",
                    "conda activate opencae"
                ],
                "icon": "terminal-powershell",
            },
            ...
    ```

- "**All Users**"の場合：

    ```json
            ...
            "PowerShell": {
                "source": "PowerShell",
                "icon": "terminal-powershell"
            },
            "Miniconda3 PowerShell": {
                "source": "PowerShell",
                "args": [
                    "-ExecutionPolicy",
                    "ByPass",
                    "-NoExit",
                    "-Command",
                    "& 'C:\\ProgramData\\miniconda3\\shell\\condabin\\conda-hook.ps1' ;",
                    "conda activate opencae"
                ],
                "icon": "terminal-powershell",
            },
            ...
    ```

設定を保存すると，画像のようにターミナルの候補にMinicondaが追加されているのが分かる．

|<img src="./figs/VSCode_Miniconda3_Terminal.png" width="70%">|
|:-----------------------------------------------------------:|


## 4. Linuxでの環境構築

私はWSL2上のOpenSUSEを用いたが，純粋Linuxや他のディストリビューションでも同様に動作するだろう．

### 4.1 Minicondaのインストール・チャネルの設定

Windowsの場合と違い，手動操作が必要．インストーラをダウンロードするため以下をターミナルで実行．
```
user@machine:~> wget -r https://repo.anaconda.com/miniconda/Miniconda3-latest-Linux-x86_64.sh
```

参考までに，標準出力は次のようになる：
```
--2023-01-12 22:00:03--  https://repo.anaconda.com/miniconda/Miniconda3-latest-Linux-x86_64.sh
Resolving repo.anaconda.com (repo.anaconda.com)... 104.16.130.3, 104.16.131.3, 2606:4700::6810:8303, ...
Connecting to repo.anaconda.com (repo.anaconda.com)|104.16.130.3|:443... connected.
HTTP request sent, awaiting response... 200 OK
Length: 72402405 (69M) [application/x-sh]
Saving to: ‘repo.anaconda.com/miniconda/Miniconda3-latest-Linux-x86_64.sh’

repo.anaconda.com/ 100%[================>]  69.05M  19.7MB/s    in 3.5s

2023-01-12 22:00:07 (19.8 MB/s) - ‘repo.anaconda.com/miniconda/Miniconda3-latest-Linux-x86_64.sh’ saved [72402405/72402405]

FINISHED --2023-01-12 22:00:07--
Total wall clock time: 3.6s
Downloaded: 1 files, 69M in 3.5s (19.8 MB/s)
```

続いて下記コマンドでインストーラ起動．

```bash
user@machine:~> bash repo.anaconda.com/miniconda/Miniconda3-latest-Linux-x86_64.sh
```

1. Enter長押し．
2. 
    ```bash
    Do you accept the license terms? [yes|no]
    [no] >>>
    ```
    に対し，`yes`．
3. 
    ```bash
    Miniconda3 will now be installed into this location:
    /home/<username>/miniconda3

    - Press ENTER to confirm the location
    - Press CTRL-C to abort the installation
    - Or specify a different location below

    [/home/<username>/miniconda3] >>>
    ```
    に対し，Enter．
4. 
    ```bash
    Do you wish the installer to initialize Miniconda3
    by running conda init? [yes|no]
    [no] >>>
    ```
    に対し，`yes`でインストール完了．ターミナルを再起動すれば，`(base)`が付いているはず．

インストーラの削除．
```bash
user@machine:~> rm -rf repo.anaconda.com
```

コンピュータを使う全ユーザから使用できるようにしたければ，下記を参考にアクセス権を付与．

- https://docs.anaconda.com/free/anaconda/install/multi-user/

```bash
#Create a new group
sudo group add mygroup
# Change the group ownership to "mygroup" on the entire directory where Anaconda is installed. 
sudo chgrp -R mygroup ~/miniconda3
# Set read and write permission for the owner, root, and the mygroup only. 
sudo chmod 770 -R ~/miniconda3
# Add users to a group. Replace USERNAME with the username of the user you are adding.
sudo adduser username mygroup
```

これ以降は，シェルスクリプトで実行できる．ライセンス違反を回避するためのチャネルの設定はWindowsの場合と全く同様に，

- `setup_miniconda.sh`

    ```bash
    conda config --remove channels defaults
    conda config --add channels conda-forge
    conda update conda -y
    ...
    ```

### 3.2 仮想環境の作成・パッケージのインストール

こちらも全く同様に，

- `setup_miniconda.sh`

    ```bash
    conda create -y -n opencae python=3.10
    conda activate opencae
    conda install -y \
        calculix \
        dataclasses-json \
        freecad \
        matplotlib \
        optuna \
        optuna-dashboard \
        plotly \
        pandas \
        requests

    conda create -y -n gui python=3.10
    conda activate gui
    conda install -y pyside6 dataclasses-json
    ```

## 5. 作業メモ

### 5.1 動作検証

- [ ] Windows
    - [x] 単一ユーザ
    - [ ] 複数ユーザ
- [ ] Linux

### 5.2 (Windows)インストール方法でMinicondaのインストール先が違う問題

少なくとも以下の4種類を確認している．

- `${env:userprofile}\miniconda3`　←wingetデフォルト
- `C:\Program Files\miniconda3`　←wingetで`--scope=machine`オプションを指定．
- `${env:userprofile}\AppData\Local\miniconda3`　←通常インストーラで自分のユーザにのみ適用
- `C:\ProgramData\miniconda3`　←通常インストーラでシステム全体に適用

まだ変わるかもしれない．やたらにtry-catchを分岐させるのも上手くないので，これらの不規則性に別の方法で対処する必要がある．

そこで，Minicondaインストール時に自動生成されるショートカット`Anaconda Prompt (Miniconda3)`のリンク先を参照することにする．

:warning: ショートカットの保存先も以下の2通りあることが確認されているが，それ以上は存在しないはずである．（また，MicrosoftがWindowsのディレクトリ構造を変更しない限り変化もしないはず...）

- `C:\ProgramData\Microsoft\Windows\Start Menu\Programs\Miniconda3 (64-bit)`
- `${env:userprofile}\AppData\Roaming\Microsoft\Windows\Start Menu\Programs\Anaconda3 (64-bit)`

追記：wingetは`--location`オプションでインストール先を指定できることが分かった．通常インストーラのインストール先に合わせれば2通りにできる．

### 5.3 (Windows)Minicondaを個人のディレクトリにインストール/システム全体にインストールした場合の権限の話

もし
```
/-----------------------------------------------------------\
|                                                           |
|    Install for:                                           |
|      ( ) Just Me (recommended)                            |
|      (x) All Users (requires admin privileges)            |
|                                                           |
\-----------------------------------------------------------/
```
こっちを選んだばあい，Minicondaのインストール，及び書各仮想環境へのパッケージインストールには**管理者権限が必要になる**．

「Just Me」のときは管理者権限必要ないのだが，場合分けして管理者昇格するようなPowerShellスクリプトの書き方が分からなかった．なので**問答無用で管理者権限を要求する**ようなスクリプトになっている．

- 無用な管理者権限を要求されるのはとても嫌な感じがするので，**なんとか場合分けできるようにしたい**．

- なんかセットアップスクリプト，エラー終了する？ウィンドウ閉じちゃうのでメッセージが分からない．一時的にでもファイルに書き出すようにしないと．

- 日本語はUFT-8だと文字化けする．．．Shift-JISは使いたくない．．．

- Windows10だとデフォルトのインストール先は`${env:userprofile}\AppData\Local\miniconda3`だが，Windows11だと`${env:userprofile}\miniconda3`になる？？
    - 場合分けが一つ増えるか

### その他memo

- [ ] step2でenvが既存の場合の分岐をつくる

- README html化の際の変更事項
    
    - ref: https://katex.org/docs/browser.html

    ```html
    <link rel="stylesheet" href="file:///c:\Users\(username)\.vscode\extensions\shd101wyy.markdown-preview-enhanced-0.7.9\crossnote\dependencies\katex\katex.min.css">
    ```

    ```html
    <link rel="stylesheet" href="https://cdn.jsdelivr.net/npm/katex@latest/dist/katex.min.css">
    ```
